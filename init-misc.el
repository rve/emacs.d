;;----------------------------------------------------------------------------
;; Misc config - yet to be placed in separate files
;;----------------------------------------------------------------------------
(add-auto-mode 'tcl-mode "Portfile\\'")
(fset 'yes-or-no-p 'y-or-n-p)

(dolist (hook (if (fboundp 'prog-mode)
                  '(prog-mode-hook ruby-mode-hook)
                '(find-file-hooks)))
  (add-hook hook 'goto-address-prog-mode))
(add-hook 'after-save-hook 'executable-make-buffer-file-executable-if-script-p)
(setq goto-address-mail-face 'link)

(setq-default regex-tool-backend 'perl)

(require-package 'sql-indent)
(eval-after-load "sql"
  '(load-library "sql-indent"))


(provide 'init-misc)



(global-linum-mode 1) ; always show line numbers                            
(setq linum-format "%d ")  ;set format
(setq scheme-program-name "scheme")
(electric-indent-mode +1) ; auto indent

(defun google ()
  "Google the selected region if any, display a query prompt otherwise."
  (interactive)
  (browse-url
   (concat
    "http://www.google.com/search?ie=utf-8&oe=utf-8&q="
    (url-hexify-string (if mark-active
         (buffer-substring (region-beginning) (region-end))
       (read-string "Google: "))))))
(global-set-key (kbd "C-c g") 'google)

;;Handy replace string
(global-set-key (kbd "C-c r") 'replace-string)
;;Handy goto line
(global-set-key (kbd "C-c j") 'goto-line)


;; color theme
;; just test rebase
